import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ProductsService } from './products.service';
import { Product as ProductsModel } from '@prisma/client';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) { }

  @Post()
  create(@Body() productData: {
    name: string;
    category: string;
    description: string;
    price: number;
  }): Promise<ProductsModel> {
    return this.productsService.create(productData)
  }

  @Get()
  findAll(): Promise<ProductsModel[]> {
    return this.productsService.findAll({
    });
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<ProductsModel> {
    return this.productsService.findOne({ id: Number(id) });
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateProduct) {
    const { name, category, description, price } = updateProduct
    return this.productsService.update({
      where: { id: Number(id) },
      data: {
        name: name,
        category: category,
        description: description,
        price: price,
      }
    });
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<ProductsModel> {
    return this.productsService.remove({ id: Number(id) });
  }
}