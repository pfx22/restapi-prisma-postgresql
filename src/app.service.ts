import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return '<h1>API REST - Products</h1><p>Status:  🟢 Online <br>Documentação: /api<br>Repositório: https://gitlab.com/pfx22/restapi-prisma-postgresql</br>Features: Swagger</p>';
  }
}
